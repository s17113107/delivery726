<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Transit by TEMPLATED</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="<?php echo base_url()?>/front/js/jquery.min.js"></script>
		<script src="<?php echo base_url()?>/front/js/skel.min.js"></script>
		<script src="<?php echo base_url()?>/front/js/skel-layers.min.js"></script>
		<script src="<?php echo base_url()?>/front/js/init.js"></script>
		<link rel="stylesheet" href="<?php echo base_url()?>/front/css/skel.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>/front/css/style.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>/front/css/style-xlarge.css" />
	</head>