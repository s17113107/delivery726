<?= view('teaching/basic/header_all')  ?>
<?= view('teaching/test_create_model') ?>
<?= view('teaching/test_edit_model') ?>
<main role="main">

    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Codeigniter 4 CRUD 範例</h1>
            <p class="lead text-muted">Something short and leading about the collection below—its contents, the creator,
                etc. Make it short and sweet, but not too short so folks don't simply skip over it entirely.</p>
            <p>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">新增新聞</button>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row" id="rowBody">
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <img class="card-img-top"
                            data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail"
                            alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                                    <button type="button" class="btn btn-sm btn-outline-secondary">Delete</button>
                                </div>
                                <small class="text-muted">9 mins</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <img class="card-img-top"
                            data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail"
                            alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                                    <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                                </div>
                                <small class="text-muted">9 mins</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <img class="card-img-top"
                            data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail"
                            alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                                    <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                                </div>
                                <small class="text-muted">9 mins</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?= view('teaching/basic/footer')  ?>
<script type="text/javascript">
    //因為ready在同一個頁面上不能有多個 所以我們使用底層的一個陣列儲存方法 載入好在一次執行
    basic.pushReady(function() {
        news.getNews();
    });
    //CRUD 範例程式碼
    let news = {
        createNews : () => {
            const data = $("#newsForm").getFormObject();
            console.log(data);
            ajaxRequset(baseUrl('example'), 'POST', data)
            .done(function(resp) {
                swal("新增成功","","success")
                .then((value) => {
                    $("#exampleModal").modal('hide');
                    $("#message").val('');
                    news.getNews();
                });
            });
        },
        getNews : () => {
            ajaxRequset(baseUrl('example'), 'GET')
            .done(function(resp) {
                let html = '';
                for (let i = 0; i < resp['data'].length; i++) {
                    html += `
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <div class="card-body">
                                <p class="card-text">${resp['data'][i]['body']}</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline-secondary" onclick="news.editNewsModal('${resp['data'][i]['hash_key']}')">修改</button>
                                        <button type="button" class="btn btn-sm btn-outline-secondary" onclick="news.deleteNews('${resp['data'][i]['hash_key']}')">刪除</button>
                                    </div>
                                    <small class="text-muted">${resp['data'][i]['date']}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                }
                $('#rowBody').html(html);
            });
        },
        editNewsModal: (key) => {
            $("#edit_hash_key").val(key);
            $("#editNewsModal").modal('show');
        },
        editNews: (key) => {
            const data = $('#editNewsForm').getFormObject();
            const hash_key = $('#edit_hash_key').val();
            ajaxRequset(baseUrl(`example/${hash_key}`), 'PUT', data)
            .done(function(resp){
                swal("修改成功","","success")
                $("#editNewsModal").modal('hide');
                news.getNews();
            });
        },
        deleteNews : (key) => {
            swal("確定要刪除嗎?","","warning")
            .then((value) => {
                const data = {
                    key: key,
                }
                ajaxRequset(baseUrl(`example/${key}`), 'DELETE')
                .done(function(resp){
                    swal("刪除成功","","success")
                    news.getNews();
                });
            });
        }
    }
</script>