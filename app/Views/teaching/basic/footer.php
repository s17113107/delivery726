		<footer class="text-muted">
			<div class="container">
				<p class="float-right">
					<a href="#">Back to top</a>
				</p>
				<p>Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
				<p>New to Bootstrap? <a href="../../">Visit the homepage</a> or read our <a
						href="../../getting-started/">getting started guide</a>.</p>
			</div>
		</footer>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			basic.documentReady.forEach(element => {
				element();
			});
		});
	</script>
</html>
