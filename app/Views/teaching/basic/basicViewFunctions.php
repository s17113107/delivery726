<!--JavaScript Load-->
<script type="text/javascript">
    //全域方法
    const baseUrl = (str) => '<?= base_url() ?>' + "/" + str;
    var defer = $.Deferred();
    //全域變數，用於儲存Modal開啟時傳遞的內容
    var viewData = {};
    //document.ready
    var basic = {
        documentReady : [],
        pushReady : function(fun){
			this.documentReady.push(fun);
		}
    }

    //全域方法，開啟彈跳視窗，並且儲存傳遞內容，傳遞內容數量可變．它們會被序列化包裝在全域變數中．
    function openModal(modalName,value=null){
        var data=[];
        for(var i=1;i<arguments.length;i++){
            data.push(arguments[i]);
        }
        viewData[modalName] = data;
        $('#'+modalName).modal('show');
    }
    //全域方法，擴充jquery，將表單序列化後製作成物件回傳
    $.fn.getFormObject = function() {
        var obj = {};
        var arr = this.serializeArray();
        console.log(arr);
        arr.forEach(function(item, index) {
            if (obj[item.name] === undefined) { // New
                obj[item.name] = item.value || '';
            } else {                            // Existing
                if (!obj[item.name].push) {
                    obj[item.name] = [obj[item.name]];
                }
                obj[item.name].push(item.value || '');
            }
        });
        return obj;
    };
    //全域方法，再次封裝ajax，定義錯誤碼；並且給予JSON．
    function ajaxRequset(url, method, postData={}) {
        return $.ajax({
            url: url,
            type: method,
            dataType: 'json',
            data: {"data" : JSON.stringify(postData)},
        }).fail(function(e) {
            swal('執行失敗', errorText(e), 'error')
        });
        //區域方法，回傳錯誤訊息
        function errorText(e){
            var text = "";
            console.log(e);
            if (e.status == 200){
                text = "無法解析伺服器回傳內容，請重新再試，若錯誤重複出現請回傳本畫面給予資訊人員。";
                text += '\n網址：'+url;
                text += '\n伺服器訊息：('+e.status+')'+"Server return data not a json-text.";
            } else if (e.status == 403){
                text = "您沒有權限瀏覽此頁面或功能，請重新再試，若錯誤重複出現請回傳本畫面給予資訊人員。";
                text += '\n網址：'+url;
                text += '\n伺服器訊息：('+e.status+')'+"Server return Forbidden.";
            } else if (e.status == 404){
                text = "找不到此頁面或功能，請重新再試，若錯誤重複出現請回傳本畫面給予資訊人員。";
                text += '\n網址：'+url;
                text += '\n伺服器訊息：('+e.status+')'+"Server return not found.";
            } else if (e.status == 500){
                text = "伺服器處理時發生錯誤，請重新再試，若錯誤重複出現請回傳本畫面給予資訊人員。";
                text += '\n網址：'+url;
                text += '\n伺服器訊息：('+e.status+')'+"Server error.";
            } else {
                text = "連線出現異常，請重新再試，若錯誤重複出現請回傳本畫面給予資訊人員。";
                text += '\n網址：'+url;
                if(e.status == "dbErrDev"){
                    text += '\n伺服器訊息：('+e.status+')'+e.statusText;
                }else{
                    text += '\n狀態碼：('+e.status+')';
                }
            }
            return text;
        }
    }
</script>