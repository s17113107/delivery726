<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="<?= base_url()?>favicon.ico">

	<title>Album example for Bootstrap</title>

	<!-- Bootstrap core CSS -->
	<link href="<?= base_url()?>/teaching/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?= base_url()?>album.css" rel="stylesheet">
	<script type="text/javascript" src="<?= base_url()?>/teaching/js/jquery-3.5.1.min.js"></script>
	<script type="text/javascript" src="<?= base_url()?>/teaching/js/popper.min.js"></script>
	<script type="text/javascript" src="<?= base_url()?>/teaching/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= base_url()?>/teaching/js/holder.min.js"></script>
	<script type="text/javascript" src="<?= base_url()?>/teaching/js/sweetalert.min.js"></script>
</head>

<body>