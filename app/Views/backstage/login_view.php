
<?= view('login/basic/header')  ?>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-26">
						Welcome
					</span>
					<span class="login100-form-title p-b-48">
						<i class="zmdi zmdi-font"></i>
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
						<input class="input100" type="text" name="account">
						<span class="focus-input100" data-placeholder="Email"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" type="button" onclick="login()">
								Login
							</button>
						</div>
					</div>

					<div class="text-center p-t-115">
						<span class="txt1">
							Don’t have an account?
						</span>

						<a class="txt2" href="#">
							Sign Up
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="dropDownSelect1"></div>
	<script>
		function login(){
            var account = $("input[name='account']").val();
            var password = $("input[name='password']").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>/backstage/login",
                data: {
                    account : account,
                    password : password
                },
                dataType: "json",
                success: function (response) {
					console.log(response);
                    if(response["status"] == "0"){
                        alert("error")
                    } else {
                        window.location.href = "<?php echo base_url() ?>/backstage/home";
                    }
                }
            });
        }
	</script>
<?= view('login/basic/footer')  ?>