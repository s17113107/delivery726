<?php namespace App\Controllers\Frontend;

use CodeIgniter\RESTful\ResourceController;
/**
 * @OA\Info(
 *   title="Frontend Restful API",
 *   version="1.0.0",
 *   @OA\Contact(
 *     email="support@example.com"
 *   )
 * )
 */
class SignUp extends ResourceController
{
    /**
	 * @OA\Post(
	 *     path="/SignUp",
	 *     summary="建立使用者",
	 * 	   description="傳入json格式資料讓API新增資料至資料庫",
	 *	   tags={"SignUp"},
	 *     @OA\RequestBody(
	 *         @OA\MediaType(
	 *             mediaType="application/x-www-form-urlencoded",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="data",
	 *                     type="object",
	 * 					   example={
	 * 							"name": "test",
	 * 							"account": "test",
	 * 							"password": "test",
	 * 							"email": "test",
	 * 							"phone": "test",
	 * 					   }
	 *                 ),
	 *
	 *             )
	 *         )
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="請求成功",
	 * 			@OA\MediaType(
	 *             mediaType="application/json",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="status",
	 *                 ),
	 *                 @OA\Property(
	 *                     property="1",
	 *                 ),
	 *                 example={"status": "1/0"}
	 *             )
	 *         )
	 *     ),
	 * )
	 */
	public function create()
	{
		$post = $this->request->getRawInput('data');
		$data = json_decode($post['data'], true);
		$result = TeachingModel::createNews($data['message']);
		return $this->response->setJSON([
			'status' => $result
		], 200);
	}
}
