<?php namespace App\Controllers\Frontend;

use App\Models\FrontendModels\MemberModel;
class Login extends BaseController
{
	public function index()
	{
		return view('frontend/login_view');
	}
	/**
	 * @OA\Post(
	 *     path="/getLoginCheck",
	 *     summary="使用者登入確認",
	 * 	   description="傳入json格式資料讓API確認使用者",
	 *	   tags={"Login"},
	 *     @OA\RequestBody(
	 *         @OA\MediaType(
	 *             mediaType="application/x-www-form-urlencoded",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="data",
	 *                     type="object",
	 * 					   example={
	 * 							"account": "test",
	 * 							"password": "test",
	 * 					   }
	 *                 ),
	 *
	 *             )
	 *         )
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="登入成功",
	 * 			@OA\MediaType(
	 *             mediaType="application/json",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="status",
	 *                 ),
	 *                 @OA\Property(
	 *                     property="1",
	 *                 ),
	 *                 example={"status": "1"}
	 *             )
	 *         )
	 *     ),
	 * 	 @OA\Response(
	 *         response=401,
	 *         description="登入失敗",
	 * 			@OA\MediaType(
	 *             mediaType="application/json",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="status",
	 *                 ),
	 *                 @OA\Property(
	 *                     property="1",
	 *                 ),
	 *                 example={"status": "0"}
	 *             )
	 *         )
	 *     ),
	 * )
	 */
	public function getLoginCheck()
	{

	}
}
