<?php namespace App\Controllers\Example;

use App\Models\TeachingModel;
use CodeIgniter\RESTful\ResourceController;
/**
 * @OA\Info(
 *   title="News Restful API",
 *   version="1.0.0",
 *   @OA\Contact(
 *     email="support@example.com"
 *   )
 * )
 */
class Teaching extends ResourceController
{
	/**
	 * @OA\GET(
	 *     path="/Example/Teaching/view",
	 *     summary="前往新聞首頁",
	 * 	   description="前往新聞首頁",
	 *	   tags={"News"},
	 *     @OA\Response(
	 *			response="200", description="An example resource"
	 *     ),
	 * )
	 */
	public function view()
	{
		return view('teaching/test_view');
	}
	/**
	 * @OA\GET(
	 *     path="/Example/Teaching",
	 *     summary="查詢新聞清單",
	 * 	   description="撈取新聞資料",
	 *	   tags={"News"},
	 *     @OA\Response(
	 *         response=200,
	 *         description="請求成功",
	 * 			@OA\MediaType(
	 *             mediaType="application/json",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="status",
	 *                 ),
	 *                 @OA\Property(
	 *                     property="1",
	 *                 ),
	 *                 example={"status": "1", "data": {
	 * 						"hash_key": "fa35e192121eabf3dabf9f5ea6abdbcbc107ac3b",
     *                      "body": "test",
     *                      "date": "2020-12-29 12:32:25"
	 * 				   }}
	 *             )
	 *         )
	 *     ),
	 * )
	 */
	public function index()
	{
		$result = TeachingModel::getNews();
		return $this->response->setJSON([
			'status' =>	"1",
			'data' => $result
		], 200);
	}
	/**
	 * @OA\GET(
	 *     path="/Example/Teaching/show/{hash_id}",
	 *     summary="查詢單一新聞",
	 * 	   description="撈取新聞資料",
	 *	   tags={"News"},
	 *     @OA\Parameter(
     *         in="path",
     *         name="hash_id",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="請求成功",
	 * 			@OA\MediaType(
	 *             mediaType="application/json",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="status",
	 *                 ),
	 *                 @OA\Property(
	 *                     property="1",
	 *                 ),
	 *                 example={"status": "200", "data": "object"}
	 *             )
	 *         )
	 *     ),
	 * )
	 */
	public function show($hash_id = NULL)
	{
		$result = TeachingModel::getNews($hash_id);
		return $this->response->setJSON([
			'status' =>	"1",
			'data' => $result
		], 200);
	}
	/**
	 * @OA\Post(
	 *     path="/Example/Teaching/create",
	 *     summary="建立新聞",
	 * 	   description="傳入json格式資料讓API新增資料至資料庫",
	 *	   tags={"News"},
	 *     @OA\RequestBody(
	 *         @OA\MediaType(
	 *             mediaType="application/x-www-form-urlencoded",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="data",
	 *                     type="object",
	 * 					   example={"message": "test"}
	 *                 ),
	 *
	 *             )
	 *         )
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="請求成功",
	 * 			@OA\MediaType(
	 *             mediaType="application/json",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="status",
	 *                 ),
	 *                 @OA\Property(
	 *                     property="1",
	 *                 ),
	 *                 example={"status": "true/false"}
	 *             )
	 *         )
	 *     ),
	 * )
	 */
	public function create()
	{
		$post = $this->request->getRawInput('data');
		$data = json_decode($post['data'], true);
		$result = TeachingModel::createNews($data['message']);
		return $this->response->setJSON([
			'status' => $result
		], 200);
	}
	/**
	 * @OA\PUT(
	 *     path="/Example/Teaching/update/{hash_id}",
	 *     summary="修改新聞",
	 * 	   description="url帶入hash_id",
	 *	   tags={"News"},
	 *     @OA\Parameter(
     *         in="path",
     *         name="hash_id",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
	 *     @OA\RequestBody(
	 *         @OA\MediaType(
	 *             mediaType="application/x-www-form-urlencoded",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="data",
	 *                     type="object",
	 * 					   example={"editMessage": "test"}
	 *                 ),
	 *
	 *             )
	 *         )
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="請求成功",
	 * 			@OA\MediaType(
	 *             mediaType="application/json",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="status",
	 *                 ),
	 *                 @OA\Property(
	 *                     property="1",
	 *                 ),
	 *                 example={"status": 1}
	 *             )
	 *         )
	 *     ),
	 * )
	 */
	public function update($hash_id = NULL)
	{
		$post = $this->request->getRawInput('data');
		$data = json_decode($post['data'], true);
		$result = TeachingModel::editNews($hash_id, $data['editMessage']);
		return $this->response->setJSON([
			'status' => $result
		], 200);
	}
	/**
	 * @OA\DELETE(
	 *     path="/Example/Teaching/delete/{hash_id}",
	 *     summary="刪除新聞",
	 * 	   description="url帶入hash_id",
	 *	   tags={"News"},
	 *     @OA\Parameter(
     *         in="path",
     *         name="hash_id",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="請求成功",
	 * 			@OA\MediaType(
	 *             mediaType="application/json",
	 *             @OA\Schema(
	 *                 @OA\Property(
	 *                     property="status",
	 *                 ),
	 *                 @OA\Property(
	 *                     property="1",
	 *                 ),
	 *                 example={"status": 1}
	 *             )
	 *         )
	 *     ),
	 * )
	 */
	public function delete($hash_id = NULL)
	{
		$result = TeachingModel::deleteNews($hash_id);
		return $this->response->setJSON([
			'status' => $result,
		], 200);
	}
}
