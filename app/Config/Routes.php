<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */
// open api
$routes->get('/backstageApi', 'Backstage\Swagger::getJSON');
$routes->get('/deliveryApi', 'Frontend\Swagger::getJSON');
$routes->get('/teachingApi', 'Example\Swagger::getJSON');


//後台route
$routes->get('/backstage', 'Backstage\Login::index');
$routes->get('/backstage/home', 'Backstage\Home::index');
//login
$routes->post('/backstage/login', 'Backstage\Login::getLoginCheck');

//前端route
$routes->get('/delivery', 'Frontend\Login::index');
$routes->get('/delivery/home', 'Frontend\Home::index');
//login
$routes->post('/delivery/logincheck', 'Frontend\Login::getLoginCheck');

//教學用
$routes->get('/', 'Example\Teaching::view');
$routes->resource('example', ['controller' => 'Example\Teaching']);
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
