<?php namespace App\Models;

use App\Models\BaseModel;
//https://codeigniter.tw/user_guide/database/examples.html [sql參考]
class TeachingModel extends BaseModel
{
	public static function createNews($message)
	{
        $sql = "INSERT INTO news (`title`, `date`, `body`) VALUES (?, ?, ?)";
		return self::getQueryStatus($sql, [
            'test',
             date('Y-m-d H:i:s'),
             $message
        ]);
    }

    public static function getNews($hash_id = NULL)
	{
        if ($hash_id !== NULL) {
            $sql = "SELECT SHA1(`id`) as hash_key, `body`, `date` FROM `news` WHERE SHA1(`id`) = ?";
            $arr = self::getQueryArray($sql, [$hash_id]);
        } else {
            $sql = "SELECT SHA1(`id`) as hash_key, `body`, `date` FROM `news`";
            $arr = self::getQueryArray($sql, []);
        }

        for ($i=0; $i < count($arr); $i++) {
            $arr[$i]['body'] = htmlspecialchars($arr[$i]['body'], ENT_QUOTES);
        }
		return $arr;
    }

    public static function editNews($key, $message)
	{
        $sql = "UPDATE news SET `body` = ? WHERE SHA1(`id`) = ?";
		return self::getQueryStatus($sql, [$message, $key]);
    }

    public static function deleteNews($key)
	{
        $sql = "DELETE FROM news WHERE SHA1(`id`) = ?";
		return self::getQueryStatus($sql, [$key]);
	}
}