<?php namespace App\Models\BackstageModels;

use CodeIgniter\Model;
//https://codeigniter.tw/user_guide/database/examples.html [sql參考]
class MemberModel extends Model
{
	public static function checkMember($account, $password)
	{
		$db = \Config\Database::connect();
		$sql = "SELECT * FROM `member` WHERE `account` = ? AND `password` = ?";
		return $db->query($sql, [$account, $password])->getResultArray();
	}
}