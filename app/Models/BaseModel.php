<?php namespace App\Models;

use CodeIgniter\Model;
//https://codeigniter.tw/user_guide/database/examples.html [sql參考]
class BaseModel extends Model
{
    private static $logPath = "/errorLog/";

    private static function writeLog($sql, $code, $msg)
    {
        $date = date('Y-m-d');
        $writePath = ROOTPATH.self::$logPath."[{$date}]dataBaseErrorLog.text";
        $fp = fopen($writePath,"a");
        $time = date('H:i:s');
        fwrite($fp,"[{$time}]{$code}-{$msg} : {$sql}\r\n");
        return fclose($fp);
    }
    /**
     * [取得資料並回傳陣列格式的資料]
     *
     * @param [type] $sql
     * @param array $data
     * @return array
     */
    protected static function getQueryArray($sql, array $data = NULL) : array
	{

        $db = \Config\Database::connect();//建立連線
        //處理資料庫錯誤;並寫入log
        try {
            return $db->query($sql, $data)->getResultArray();//執行SQL語法
        } catch (\Exception $e) {
            self::writeLog($db->getLastQuery(), $e->getCode(), $e->getMessage());
            return [
                "errorCode" => $e->getCode()
            ];
        }
    }
    /**
     * [執行SQL語法並回傳執行後狀態]
     *
     * @param [type] $sql
     * @param array $data
     * @return string 1
     */
    protected static function getQueryStatus($sql, array $data = NULL)
	{
        $db = \Config\Database::connect();//建立連線
        //處理資料庫錯誤;並寫入log
        try {
            return $db->query($sql, $data)->connID->affected_rows;//執行SQL語法
        } catch (\Exception $e) {
            self::writeLog($db->getLastQuery(), $e->getCode(), $e->getMessage());
            return [
                "errorCode" => $e->getCode()
            ];
        }
    }
    /**
     * [執行SQL語法並回傳新增後的主鍵]
     *
     * @param [type] $sql
     * @param array $data
     * @return string insertId
     */
    protected static function getQueryInsertId($sql, array $data = NULL)
	{
        $db = \Config\Database::connect();//建立連線
        //處理資料庫錯誤;並寫入log
        try {
            return $db->query($sql, $data)->connID->insertID;//執行SQL語法
        } catch (\Exception $e) {
            self::writeLog($db->getLastQuery(), $e->getCode(), $e->getMessage());
            return [
                "errorCode" => $e->getCode()
            ];
        }
    }
}